/**
 * Copyright (c) 2017 John Waweru Wambugu
 *
 * @license MIT
 * created by waweru
 *
 * WARNING: this module is meant for the purpose of knowing how the list came to be running this
 * module might cause the library to break due to miss diagnosis of which algorithm was used.
 * This file is useful for the purpose of determining which algorithm can work with the library not for
 * the library itself.
 */

import {getCiphers} from 'crypto';
import {writeFile} from 'fs';
import {floor, isEqual, pull, random} from 'lodash';
import {decrypt, encrypt} from '../index';

let ciphers: string[] = getCiphers(); // list of all encryption algorithms
const key: string = 'somelongweirdkeystring-with-numbers_like-1176734858$$'; // custom encryption key
// contains sample data-types for testing the various algorithms
const data: any[] = [
  'iam_a_peaceful_person_getting_through_life_peacefully',
  {name: 'James', gender: 'male', age: '21', bio: 'simple kid with nothing to hide. A nobody.'},
  901208823742213,
  'I_mix_numbers-with-1238197231_and_add-few-signs-$$%^',
  JSON.stringify({name: 'James', gender: 'male', age: '21', bio: 2891728193271846917, mean: '(*&^%$#@#$%^&*())'}),
  [
    {name: 'James', gender: 'male'},
    {name: 'Rachel', gender: 'female'},
    {name: 'Belle', gender: 'female'},
    {name: 'Don', gender: 'male'}
  ]
];

/**
 *
 * @param arr
 * @param n
 *
 * randomly select an algorithm
 */
const pickAlgorithm = (arr: string[], n: number): any => {
  const idx: number = floor(random(n-2));
  return arr[idx];
};

/**
 *
 * @param cipher
 *
 * test the selected algorithm
 */
const testCipher = (cipher: string) => {
  return data.reduce((acc: boolean, curr: string) => {
    try {
      const encryptedData: string = encrypt<any>(curr, key, cipher);
      const decryptedData: any = decrypt<any>(encryptedData, key, cipher);
      return isEqual(curr, decryptedData);
    } catch (e) {
      return false;
    }
  }, 'aes-256-ctr');
};

/**
 *
 * @param output
 * @param cb
 *
 * write to a json file the list of all algorithms that work well with the encryption function
 */
const outputJsonFile = (output: string[], cb: any) => {
    writeFile(`.list.json`, JSON.stringify({ ciphers: [...output] }),
    (err) => {
        if (err) {
            throw err;
        } else {
            cb();
        }
    });
};

/**
 * function generates the list of all possible algorithms
 * test them to see if they work with the library and output a file with all those algorithms.
 */
function generate(): void {
    const workingCiphers: string[] = [];
    let count: number = 0;
    // tslint:disable-next-line:no-console
    console.log('generating list of random algorithms');
    const generator: any = setInterval(() => {
        const alg: string = pickAlgorithm(ciphers, ciphers.length);
        // tslint:disable-next-line:no-console
        console.log(`\x1b[33m \t testing: ${alg}`);

        const test = (cipher: string): void => {
            if (testCipher(cipher)) {
                // tslint:disable-next-line:no-console
                console.log('\x1b[32m \t', cipher, 'passed!');
                workingCiphers.push(cipher);
                count++;
            } else {
                // tslint:disable-next-line:no-console
                console.log('\x1b[31m \t', cipher, 'failed!');
            }
        };

        // to avoid Segmentation fault (core dumped) we limit only to few types of algorithms
        switch (alg[0]) {
            case 'a':
              test(alg);
              break;
            case 'r':
              test(alg);
              break;
            case 'b':
              test(alg);
              break;
        }

        ciphers=pull(ciphers, alg); // pluck the already tested algorithm

        if (count === 20) {
            outputJsonFile(workingCiphers, () => {
                clearInterval(generator);
                // tslint:disable-next-line:no-console
                console.log(`\x1b[34m Process Completed. Output on the file '.list.json'`);
            });
        }
    }, 150);
}

generate();
